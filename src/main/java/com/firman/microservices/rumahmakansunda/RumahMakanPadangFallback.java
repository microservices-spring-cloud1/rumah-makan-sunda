package com.firman.microservices.rumahmakansunda;

import org.springframework.stereotype.Component;

@Component
public class RumahMakanPadangFallback implements RumahMakanPadangService {

    @Override
    public String helloPadang() {
        return null;
    }
}
