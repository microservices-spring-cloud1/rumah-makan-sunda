package com.firman.microservices.rumahmakansunda;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "rumah-makan-padang", fallback = RumahMakanPadangFallback.class)
public interface RumahMakanPadangService {

    @RequestMapping(method = RequestMethod.GET, value = "/rumah-makan-padang/hello/1")
    public String helloPadang();

}
