package com.firman.microservices.rumahmakansunda;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@SpringBootApplication
@EnableFeignClients
@EnableCircuitBreaker
@EnableHystrixDashboard
@EnableZuulProxy
@RestController
public class RumahMakanSundaApplication {

	public static void main(String[] args) {
		SpringApplication.run(RumahMakanSundaApplication.class, args);
	}

	@Autowired private RumahMakanPadangService rumahMakanPadangService;

	@GetMapping("/helloSunda")
	public ResponseEntity<?> helloSunda(){
		return new ResponseEntity<String>("Hello Rumah Makan Sunda", HttpStatus.OK);
	}

	@GetMapping("/helloPadang")
	public ResponseEntity<?> helloPadang(){
		String response = rumahMakanPadangService.helloPadang();
		if(response != null && !"".equals(response.trim())){
			return new ResponseEntity<String>(response, HttpStatus.OK);
		}
		return new ResponseEntity<String>("Hello Padang oy", HttpStatus.OK);
	}

}
