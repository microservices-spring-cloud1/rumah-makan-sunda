CREATE TABLE users (
    id bigint NOT NULL,
    username varchar(100) NOT NULL,
    first_name varchar(50) NOT NULL,
    last_name varchar(50) DEFAULT NULL,
    PRIMARY KEY (id)
);

CREATE SEQUENCE user_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;